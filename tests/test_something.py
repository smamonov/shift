import pytest
from httpx import AsyncClient

from .variables import Vars as var
from main import fake_db

@pytest.mark.asyncio
async def test_not_auth(ac: AsyncClient):
    '''
    Проверка эдроинта /salary без авторизации
    '''
    response = await ac.get('http://test/salary')
    assert response.status_code == 401, 'wrong response'


@pytest.mark.asyncio
async def test_wrong_pass(ac):
    '''
    Попытка авторизации с некорректным паролем
    '''
    response = await ac.post('http://test/login', data={"username": var.username, "password": "incorrect_pass"},)
    assert response.status_code == 401, 'wrong response'


@pytest.mark.asyncio
async def test_auth_and_get_token(ac: AsyncClient):
    '''
    Попытка авторизации с корректными данными
    '''
    response = await ac.post('http://test/login', data={"username": var.username, "password": var.password},)
    var.token = response.json()['access_token']
    assert response.status_code == 200, 'wrong response'


@pytest.mark.asyncio
async def test_auth(ac: AsyncClient):
    '''
    Проверка эдроинта /salary с авторизацией
    '''
    response = await ac.get('http://test/salary', headers={"Authorization": f"Bearer {var.token}"})
    response_data = response.json()
    assert response.status_code == 200, 'wrong response'
    assert response_data['Coworker'] == fake_db[var.username]['full_name'], "wrong full name"
    assert response_data['salary'] == fake_db[var.username]['salary'], "wrong salary"
    assert response_data['date_salary_increase'] == fake_db[var.username]['date_salary_increase'], "wrong date salary increase"

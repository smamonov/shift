import pytest_asyncio
from httpx import AsyncClient

from main import app


@pytest_asyncio.fixture
async def ac():
    async with AsyncClient(app=app) as ac:
        yield ac
